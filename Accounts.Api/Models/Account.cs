﻿namespace Accounts.Api.Models
{
    public class Account
    {
        public int Id { get; set; }
        public int IdCustomer { get; set; }

        public int IdProduct { get; set; }
        public string AccountNo { get; set; }
        public string Cabang { get; set; }
        public string Address { get; set; }
        public DateTime DateCreated { get; set; }

    }
}
