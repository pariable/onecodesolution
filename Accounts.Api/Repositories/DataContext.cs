﻿using Accounts.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Accounts.Api.Repositories;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions options) : base(options) { }

    public DbSet<Account> Account { get; set; }
}
