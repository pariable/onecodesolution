﻿using Accounts.Api.Models;
using Accounts.Api.Repositories;

namespace Accounts.Api.Services;

public class AccountService
{
    private readonly DataContext db;

    public AccountService(DataContext db)
    {
        this.db = db;
    }

    public Account Get(int id)
    {
        return db.Account.Find(id);
    }

    public List<Account> GetList()
    {
        return db.Account.ToList();
    }

    public Account Create(Account customer)
    {
        db.Account.Add(customer);
        db.SaveChanges();
        return customer;
    }

    public Account Update(Account customer)
    {
        db.Account.Update(customer);
        db.SaveChanges();
        return customer;
    }

    public void Delete(int id)
    {
        var customer = db.Account.Find(id);
        db.Account.Remove(customer);
        db.SaveChanges();
    }
}
