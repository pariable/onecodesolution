﻿using Products.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Products.Api.Repositories;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions options) : base(options) { }

    public DbSet<Product> Product { get; set; }
}
