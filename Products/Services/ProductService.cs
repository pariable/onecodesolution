﻿using Products.Api.Models;
using Products.Api.Repositories;

namespace Products.Api.Services;

public class ProductService
{
    private readonly DataContext db;

    public ProductService(DataContext db)
    {
        this.db = db;
    }

    public Product Get(int id)
    {
        return db.Product.Find(id);
    }

    public List<Product> GetList()
    {
        return db.Product.ToList();
    }

    public Product Create(Product product)
    {
        db.Product.Add(product);
        db.SaveChanges();
        return product;
    }

    public Product Update(Product product)
    {
        db.Product.Update(product);
        db.SaveChanges();
        return product;
    }

    public void Delete(int id)
    {
        var product = db.Product.Find(id);
        db.Product.Remove(product);
        db.SaveChanges();
    }
}
