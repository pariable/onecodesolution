﻿using Products.Api.Models;
using Products.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Products.Api.Controllers;

[Route("api/v1/product")]
[ApiController]
public class ProductController : ControllerBase
{
    private readonly ProductService productService;

    public ProductController(ProductService productService)
    {
        this.productService = productService;
    }

    [HttpGet("{id}")]
    public ActionResult<Product> Get(int id)
    {
        var result = productService.Get(id);
        return Ok(result);
    }

    [HttpGet]
    public ActionResult<List<Product>> GetList()
    {
        var result = productService.GetList();
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<Product> Create([FromBody] Product product)
    {
        var result = productService.Create(product);
        return CreatedAtAction(nameof(Get), new { result.Id }, product);
    }

    [HttpPut("{id}")]
    public ActionResult<Product> Update(int id, [FromBody] Product product)
    {
        product.Id = id;
        var result = productService.Update(product);
        return Ok(result);
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        productService.Delete(id);
        return NoContent();
    }
}
