﻿using Mandiri.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Mandiri.Api.Repositories;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions options) : base(options) { }

    public DbSet<Account> Account { get; set; }

    public DbSet<AccountBalance> AccountBalance { get; set; }

    public DbSet<Customer> Customer { get; set; }
}
