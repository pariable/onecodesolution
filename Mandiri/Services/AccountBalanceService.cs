﻿using Mandiri.Api.Models;
using Mandiri.Api.Repositories;

namespace Mandiri.Api.Services;

public class AccountBalanceService
{
    private readonly DataContext db;

    public AccountBalanceService(DataContext db)
    {
        this.db = db;
    }

    public AccountBalance Get(int id)
    {
        return db.AccountBalance.Find(id);
    }

    public List<AccountBalance> GetList()
    {
        return db.AccountBalance.ToList();
    }

    public AccountBalance Create(AccountBalance accountbalance)
    {
        db.AccountBalance.Add(accountbalance);
        db.SaveChanges();
        return accountbalance;
    }

    public AccountBalance Update(AccountBalance accountbalance)
    {
        db.AccountBalance.Update(accountbalance);
        db.SaveChanges();
        return accountbalance;
    }

    public void Delete(int id)
    {
        var accountbalance = db.AccountBalance.Find(id);
        db.AccountBalance.Remove(accountbalance);
        db.SaveChanges();
    }
}
