﻿using Mandiri.Api.Models;
using Mandiri.Api.Repositories;

namespace Mandiri.Api.Services;

public class AccountService
{
    private readonly DataContext db;

    public AccountService(DataContext db)
    {
        this.db = db;
    }

    public Account Get(int id)
    {
        return db.Account.Find(id);
    }

    public List<Account> GetList()
    {
        return db.Account.ToList();
    }

    public Account Create(Account account)
    {
        db.Account.Add(account);
        db.SaveChanges();
        return account;
    }

    public Account Update(Account account)
    {
        db.Account.Update(account);
        db.SaveChanges();
        return account;
    }

    public void Delete(int id)
    {
        var account = db.Account.Find(id);
        db.Account.Remove(account);
        db.SaveChanges();
    }
}
