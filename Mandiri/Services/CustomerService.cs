﻿using Mandiri.Api.Models;
using Mandiri.Api.Repositories;

namespace Mandiri.Api.Services;

public class CustomerService
{
    private readonly DataContext db;

    public CustomerService(DataContext db)
    {
        this.db = db;
    }

    public Customer Get(int id)
    {
        return db.Customer.Find(id);
    }

    public List<Customer> GetList()
    {
        return db.Customer.ToList();
    }

    public Customer Create(Customer customer)
    {
        db.Customer.Add(customer);
        db.SaveChanges();
        return customer;
    }

    public Customer Update(Customer customer)
    {
        db.Customer.Update(customer);
        db.SaveChanges();
        return customer;
    }

    public void Delete(int id)
    {
        var customer = db.Customer.Find(id);
        db.Customer.Remove(customer);
        db.SaveChanges();
    }
}
