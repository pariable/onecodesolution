﻿namespace Mandiri.Api.Models;

public class AccountBalance
{
    public int Id { get; set; }
    public string AccountNo { get; set; }
    public string Description { get; set; }
    public Double Debet { get; set; }
    public Double Credit { get; set; }
    public DateTime DateCreated { get; set; }
}
