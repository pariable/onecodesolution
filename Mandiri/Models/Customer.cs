﻿namespace Mandiri.Api.Models;

public class Customer
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Gender { get; set; }
    public string Address { get; set; }
    public string Religion { get; set; }
    public DateTime DateCreated { get; set; }
}
