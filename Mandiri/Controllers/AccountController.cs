﻿using Mandiri.Api.Models;
using Mandiri.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Mandiri.Api.Controllers;

[Route("account")]
[ApiController]
public class AccountController : ControllerBase
{
    private readonly AccountService accountService;

    public AccountController(AccountService accountService)
    {
        this.accountService = accountService;
    }

    [HttpGet("{id}")]
    public ActionResult<Account> Get(int id)
    {
        var result = accountService.Get(id);
        return Ok(result);
    }

    [HttpGet]
    public ActionResult<List<Account>> GetList()
    {
        var result = accountService.GetList();
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<Account> Create([FromBody] Account account)
    {
        var result = accountService.Create(account);
        return CreatedAtAction(nameof(Get), new { result.Id }, account);
    }

    [HttpPut("{id}")]
    public ActionResult<Account> Update(int id, [FromBody] Account account)
    {
        account.Id = id;
        var result = accountService.Update(account);
        return Ok(result);
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        accountService.Delete(id);
        return NoContent();
    }
}
