﻿using Mandiri.Api.Models;
using Mandiri.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Mandiri.Api.Controllers;

[Route("accountbalance")]
[ApiController]
public class AccountBalanceController : ControllerBase
{
    private readonly AccountBalanceService accountbalanceService;

    public AccountBalanceController(AccountBalanceService accountbalanceService)
    {
        this.accountbalanceService = accountbalanceService;
    }

    [HttpGet("{id}")]
    public ActionResult<AccountBalance> Get(int id)
    {
        var result = accountbalanceService.Get(id);
        return Ok(result);
    }

    [HttpGet]
    public ActionResult<List<AccountBalance>> GetList()
    {
        var result = accountbalanceService.GetList();
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<AccountBalance> Create([FromBody] AccountBalance accountbalance)
    {
        var result = accountbalanceService.Create(accountbalance);
        return CreatedAtAction(nameof(Get), new { result.Id }, accountbalance);
    }

    [HttpPut("{id}")]
    public ActionResult<AccountBalance> Update(int id, [FromBody] AccountBalance accountbalance)
    {
        accountbalance.Id = id;
        var result = accountbalanceService.Update(accountbalance);
        return Ok(result);
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        accountbalanceService.Delete(id);
        return NoContent();
    }
}
