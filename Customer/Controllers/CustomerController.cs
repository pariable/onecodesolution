﻿using Customers.Api.Models;
using Customers.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Customers.Api.Controllers;

[Route("api/v1/customer")]
[ApiController]
public class CustomerController : ControllerBase
{
    private readonly CustomerService customerService;

    public CustomerController(CustomerService customerService)
    {
        this.customerService = customerService;
    }

    [HttpGet("{id}")]
    public ActionResult<Customer> Get(int id)
    {
        var result = customerService.Get(id);
        return Ok(result);
    }

    [HttpGet]
    public ActionResult<List<Customer>> GetList()
    {
        var result = customerService.GetList();
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<Customer> Create([FromBody] Customer customer)
    {
        var result = customerService.Create(customer);
        return CreatedAtAction(nameof(Get), new { result.Id }, customer);
    }

    [HttpPut("{id}")]
    public ActionResult<Customer> Update(int id, [FromBody] Customer customer)
    {
        customer.Id = id;
        var result = customerService.Update(customer);
        return Ok(result);
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        customerService.Delete(id);
        return NoContent();
    }
}
