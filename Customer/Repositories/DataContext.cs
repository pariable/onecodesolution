﻿using Customers.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Customers.Api.Repositories;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions options) : base(options) { }

    public DbSet<Customer> Customer { get; set; }
}
